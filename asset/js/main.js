(function ($) {
	"use strict";

    jQuery(document).ready(function($){
      
         /*
         <<=========Isotop========>>
         */
        $(".project-title li").on('click', function() {
             var selector = $(this).attr('data-filter');
           $(".projects-lists").isotope({
                filter: selector
           });
        });
        
         /*
         <<=========Magnifing Pop Up========>>
         */
            $('.projects-lists').magnificPopup({
              delegate: 'a',
              type: 'image',
              tLoading: 'Loading image #%curr%...',
              mainClass: 'mfp-img-mobile',
              gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] 
              },
              image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function(item) {

                }
              }
            });
		
			
          
        
         /*
         <<=========Active Menu========>>
         */
        $('.project-title li').click(function(){
            $('.project-title li').removeClass("active");
            $(this).addClass("active");
        });
        
        
         /*
         <<=========wow js========>>
         */
        new WOW().init();
        
												// Configuration
  
        
        
       
        



        

    });
      
      


    jQuery(window).load(function(){
        
       
         /*
         <<=========Isotop========>>
         */
        
         jQuery(".projects-lists").isotope();
		 
		 

              
    });
    
    
    
    
 

    
    
    


}(jQuery));	